<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\OtpCode;
use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp'   => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otpCode = OtpCode::where('otp', $request->otp)->first();
        $user = $otpCode->user;


        if (!$otpCode->valid_until > Carbon::now()) {
            return response()->json([
                'succes' => false,
                'message' => 'OTP Code sudah kadaluarsa'
                ], 404);
            }
        
        
        $user->update([
            'email_verified_at' => Carbon::now()
        ]);

        $otpCode->delete();

        return response()->json([
            'success' => true,
            'message' => 'Selamat! Akun anda terverifikasi.',
            'data' => $user
        ]);
    }
}